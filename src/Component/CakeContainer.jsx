import React from 'react';
import { useSelector,useDispatch } from 'react-redux';
import { buyCake } from '../redux/Cake/CakeAction';

const CakeContainer = () => {

    const numOfCakes = useSelector(state => state.numOfCakes)

    const dispatch = useDispatch();

    return (
        <>
        <h1>Number of Cakes {numOfCakes}</h1>
        <button onClick={()=> dispatch(buyCake())}>Buy cakes</button>
        </>
    )

}

export default CakeContainer;